#!/usr/bin/env bash
clear
while true
do
  printf "MENU D'ACTIONS - TP ABR-AVL\n
\t * P -- Lancer abr pour les tests des fonctions pour les arbres ABR\n
\t * O -- Lancer avl pour les tests des fonctions pour les arbres AVL\n
\t * N -- Effectuer \"un make clean\" afin de nettoyer les fichiers générés par la compilation\n
\t * Q -- Quitter le menu\n

Entrez une lettre (parmi les choix possibles) puis appuyez sur Entrée : "

  #..........................................................................
  # saisie d une touche et gestion
  #..........................................................................
  read answer
  clear

  case "$answer" in
    [Pp]*) make abr;
	   clear;
           echo "Saisissez le nom du premier fichier d'arbre" ; read f1;
           echo "Saisissez le nom du second fichier d'arbre" ; read f2 ;
           printf "************************TEST ABR*******************************\n\n";
           ./abr "$f1" "$f2";;
    [Nn]*) make clean;;
    [Oo]*) make avl;
	   clear;
	   echo "Saisissez le nom du premier fichier d'arbre" ; read f1;
           echo "Saisissez le nom du second fichier d'arbre" ; read f2 ;
           printf "************************TEST AVL*******************************\n\n";
           ./avl "$f1" "$f2";;
    [Qq]*)  echo "Sortie du Programme" ; exit 0 ;;
    *)      echo "Choisissez une option affichee dans le menu" ;;
  esac
  echo ""
  echo "Appuyez sur Entrée pour un retour au menu"
  read dummy
done
