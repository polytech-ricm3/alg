#include <stdio.h>
#include <stdlib.h>

#include "avl.h"
#include "pile.h"
#include "file.h"


#define max(a,b) ((a)>(b)?(a):(b))
#define min(a,b) ((a)<(b)?(a):(b))

int balance(Arbre_t a)
{
  return (a == NULL) ? 0 : hauteur_arbre_r(a->fgauche) - hauteur_arbre_r(a->fdroite);
}

Arbre_t rotation_gauche(Arbre_t a)
{
  Arbre_t b = a->fdroite;
  Arbre_t tmp = b->fgauche;

  b->fgauche = a;
  a->fdroite = tmp;

  a->bal = balance(a);
  b->bal = balance(b);

  return b;
}

Arbre_t rotation_droite(Arbre_t a)
{
  Arbre_t b = a->fgauche;
  Arbre_t tmp = b->fdroite;

  b->fdroite = a;
  a->fgauche = tmp;

  a->bal = balance(a);
  b->bal = balance(b);

  return b;
}

int feuille (Arbre_t a)
{
  if (a == NULL)
    return 0;
  else
  {
    if ((a->fgauche == NULL) && (a->fdroite == NULL))
      return 1 ;
    else
      return 0 ;
  }
}

Arbre_t ajouter_noeud (Arbre_t a, Arbre_t n)
{
  if (a == NULL)
  {
    n->bal = 0;
    return n ;
  }
  else if (n->cle < a->cle)
  {
    a->fgauche = ajouter_noeud (a->fgauche, n) ;
  }
  else if(n->cle > a->cle)
  {
    a->fdroite = ajouter_noeud (a->fdroite, n) ;
  }
  else
    return a;

  a->bal = balance(a);

  if(a->bal > 1 && n->cle < a->fgauche->cle)
    return rotation_droite(a);

  if(a->bal < -1 && n->cle > a->fdroite->cle)
    return  rotation_gauche(a);

  if(a->bal > 1 && n->cle > a->fgauche->cle)
  {
    a->fgauche = rotation_gauche(a->fgauche);
    return rotation_droite(a);
  }

  if(a->bal < -1 && n->cle < a->fdroite->cle)
  {
    a->fdroite = rotation_droite(a->fdroite);
    return rotation_gauche(a);
  }
  return a;
}

Arbre_t rechercher_cle_arbre (Arbre_t a, int valeur)
{
  if (a == NULL)
    return NULL ;
  else
  {
    if (a->cle == valeur)
    return a ;
    else
    {
      if (a->cle < valeur)
        return rechercher_cle_arbre (a->fdroite, valeur) ;
      else
        return rechercher_cle_arbre (a->fgauche, valeur) ;
    }
  }
}

Arbre_t ajouter_cle (Arbre_t a, int cle)
{
  Arbre_t n ;
  Arbre_t ptrouve ;

  /*
  ajout de la clé. Creation du noeud n qu'on insere
  dans l'arbre a
  */

  ptrouve = rechercher_cle_arbre (a, cle) ;

  if (ptrouve == NULL)
  {
    n = (Arbre_t) malloc (sizeof(noeud_t)) ;
    n->cle = cle;
    n->fgauche = NULL ;
    n->fdroite = NULL ;

    a = ajouter_noeud (a, n) ;
    return a ;
  }
  else
  return a ;
}

Arbre_t lire_arbre (char *nom_fichier)
{
  FILE *f ;
  int cle;
  Arbre_t a = NULL;

  f = fopen (nom_fichier, "r") ;

  while (fscanf (f, "%d", &cle) != EOF)
  {
    a = ajouter_cle (a, cle) ;
  }

  fclose (f) ;

  return a ;
}

void afficher_arbre (Arbre_t a, int niveau)
{
  /*
  affichage de l'arbre a
  on l'affiche en le penchant sur sa gauche
  la partie droite (haute) se retrouve en l'air
  */

  int i ;

  if (a != NULL)
  {
    afficher_arbre (a->fdroite,niveau+1) ;

    for (i = 0; i < niveau; i++)
      printf ("\t") ;
    printf (" %d [%d][%d]\n\n", a->cle, niveau,a->bal) ;

    afficher_arbre (a->fgauche, niveau+1) ;
  }

  return ;
}


int hauteur_arbre_r (Arbre_t a)
{
  if (a == NULL)
    return 0;

  return max(hauteur_arbre_r(a->fdroite)+1, hauteur_arbre_r(a->fgauche)+1) ;
}

int hauteur_arbre_nr (Arbre_t a)
{
  if(a == NULL)
    return 0;

  int hauteur = 0;
  pfile_t f = creer_file();

  deposer_file(f,a);

  while(!file_vide(f))
  {

    hauteur++;
    int nb_noeuds_niveau = f->nb_elem;

    while(nb_noeuds_niveau > 0)
    {
      pnoeud_t n = retirer_file(f);

      if(n->fgauche != NULL)
        deposer_file(f,n->fgauche);

      if(n->fdroite != NULL)
        deposer_file(f,n->fdroite);

      nb_noeuds_niveau--;
    }
  }
  return hauteur;
}


void parcourir_arbre_largeur (Arbre_t a)
{
  if(a == NULL)
    return;

  pfile_t f = creer_file();
  pnoeud_t n;

  deposer_file(f,a);
  printf("Parcours en largeur : ");

  while(!file_vide(f))
  {
    n = retirer_file(f);
    printf("%d ",n->cle);

    if(n->fgauche != NULL)
      deposer_file(f,n->fgauche);

    if(n->fdroite != NULL)
      deposer_file(f,n->fdroite);
  }
  printf("\n");
}

void afficher_nombre_noeuds_par_niveau (Arbre_t a)
{
  if(a == NULL)
  {
    printf("Nb Noeuds Niveau %d : %d\n",0,0);
    return;
  }

  int niveau_courant = 0;
  int nb_noeuds_niveau = 0;
  pfile_t f = creer_file();

  deposer_file(f,a);

  while(!file_vide(f))
  {
    nb_noeuds_niveau = f->nb_elem;

    printf("Nb Noeuds Niveau %d : %d\n",niveau_courant,nb_noeuds_niveau);
    niveau_courant++;

    while(nb_noeuds_niveau > 0) {
      pnoeud_t n = retirer_file(f);

      if(n->fgauche != NULL)
        deposer_file(f,n->fgauche);

      if(n->fdroite != NULL)
        deposer_file(f,n->fdroite);

      nb_noeuds_niveau--;
    }
  }
}


int nombre_cles_arbre_r (Arbre_t a)
{
  if (a == NULL)
    return 0;

  int nb = 1;
  nb += (nombre_cles_arbre_r(a->fgauche) +  nombre_cles_arbre_r(a->fdroite));
  return nb;
}

int nombre_cles_arbre_nr (Arbre_t a)
{
  int nb = 0;
  if(a != NULL)
  {
    ppile_t p = creer_pile();
    empiler(p,a);

    while(!pile_vide(p))
    {
      pnoeud_t n = depiler(p);

      if(n != NULL)
      {
        nb++;
        empiler(p, n->fgauche);
        empiler(p,n->fdroite);
      }

    }
  }
  return nb;
}

int trouver_cle_min (Arbre_t a)
{
  if (a == NULL)
    return 0;

  if (a->fgauche == NULL)
    return a->cle;

  return trouver_cle_min(a->fgauche);
}



void imprimer_liste_cle_triee_r (Arbre_t a)
{
  if (a == NULL)
    return;

  imprimer_liste_cle_triee_r(a->fgauche);
  printf("%d ", a->cle);
  imprimer_liste_cle_triee_r(a->fdroite);
}

void imprimer_liste_cle_triee_nr (Arbre_t a)
{
  ppile_t p = creer_pile();
  pnoeud_t n = a;
  printf("Imprimer liste cles triees nr : ");

  while (n != NULL || !pile_vide(p))
  {
    while (n !=  NULL)
    {
      empiler(p,n);
      n = n->fgauche;
    }
    n = depiler(p);
    printf("%d ",n->cle);
    n = n->fdroite;
  }
  printf("\n");
}


int arbre_parfait (Arbre_t a)
{
  int ld,rd;

  if (a == NULL)
  return 1;

  ld=hauteur_arbre_r(a->fgauche);
  rd=hauteur_arbre_r(a->fdroite);

  return(ld==rd && arbre_parfait(a->fgauche) && arbre_parfait(a->fdroite));
}

Arbre_t rechercher_cle_sup_arbre (Arbre_t a, int valeur)
{
  ppile_t p = creer_pile();
  pnoeud_t n = a;

  while (n != NULL || !pile_vide(p))
  {
    while (n !=  NULL)
    {
      empiler(p,n);
      n = n->fgauche;
    }
    n = depiler(p);

    if(n->cle > valeur)
      return n;

    n = n->fdroite;
  }
  return NULL;
}

Arbre_t rechercher_cle_inf_arbre (Arbre_t a, int valeur)
{
  ppile_t p = creer_pile();
  pnoeud_t n = a;

  while (n != NULL || !pile_vide(p))
  {
    while (n !=  NULL)
    {
      empiler(p,n);
      n = n->fdroite;
    }
    n = depiler(p);

    if(n->cle < valeur)
      return n;

    n = n->fgauche;
  }
  return NULL;
}

Arbre_t detruire_cle_arbre (Arbre_t a, int cle)
{
  if(a == NULL)
    return a;

  if(cle < a->cle)
    a->fgauche = detruire_cle_arbre(a->fgauche,cle);
  else if (cle > a-> cle)
    a->fdroite = detruire_cle_arbre(a->fdroite,cle);
  else
  {
    if(a->fgauche == NULL || a->fdroite == NULL)
    {
      Arbre_t tmp = a->fgauche ? a->fgauche : a->fdroite;
      if(tmp == NULL)
      {
        tmp = a;
        a = NULL;
      }
      else
        a = tmp;
      free(tmp);
    }
    else
    {
      int tmp = trouver_cle_min(a->fdroite);
      a->cle = tmp;
      a->fdroite = detruire_cle_arbre(a->fdroite,tmp);
    }

    if(a == NULL)
      return a;

    a->bal = balance(a);

    if(a->bal > 1 && balance(a->fgauche) >= 0)
      return rotation_droite(a);

    if(a->bal > 1 && balance(a->fgauche) < 0) {
      a->fgauche = rotation_gauche(a);
      return rotation_droite(a);
    }

    if (a->bal < -1 && balance(a->fdroite) <= 0) {
      return rotation_gauche(a);
    }

    if(a->bal < -1 && balance(a->fgauche) > 0) {
      a->fdroite = rotation_droite(a->fdroite);
      return rotation_gauche(a);
    }
  }
  return a;
}



Arbre_t intersection_deux_arbres (Arbre_t a1, Arbre_t a2)
{
  if(a1 == NULL || a2 == NULL)
    return NULL;

  Arbre_t res = NULL;
  ppile_t p,q;
  p = creer_pile();
  q = creer_pile();

  pnoeud_t n1 = a1, n2 = a2;

  while(1)
  {
    if(n1 != NULL)
    {
      empiler(p,n1);
      n1 = n1->fgauche;
    }
    else if(n2 != NULL)
    {
      empiler(q,n2);
      n2 = n2->fgauche;
    }
    else if (!pile_vide(p) && !pile_vide(q))
    {
      n1 = p->Tab[p->sommet];
      n2 = q->Tab[q->sommet];

      if(n1->cle < n2->cle)
      {
        depiler(p);
        n1 = n1->fdroite;
        n2 = NULL;
      }
      else if(n2->cle < n1->cle)
      {
        depiler(q);
        n2 = n2->fdroite;
        n1 = NULL;
      }
      else
      {
        res = ajouter_cle(res,n1->cle);
        depiler(p);
        depiler(q);
        n1 = n1->fdroite;
        n2 = n2->fdroite;
      }
    }
    else
      break;
  }
  return res;
}

Arbre_t union_deux_arbres (Arbre_t a1, Arbre_t a2)
{
  Arbre_t res = NULL;
  ppile_t p = creer_pile();
  empiler(p,a2);

  while(!pile_vide(p))
  {
    pnoeud_t n = depiler(p);

    if(n != NULL)
    {
      res = ajouter_cle(res,n->cle);
      empiler(p, n->fgauche);
      empiler(p,n->fdroite);
    }
  }
  empiler(p,a1);
  while(!pile_vide(p))
  {
    pnoeud_t n = depiler(p);

    if(n != NULL)
    {
      res = ajouter_cle(res,n->cle);
      empiler(p, n->fgauche);
      empiler(p,n->fdroite);
    }
  }
  return res;
}

int main (int argc, char**argv)
{
  Arbre_t a, b ;

  if (argc < 2)
  {
    fprintf (stderr, "il manque le parametre nom de fichier\n") ;
    exit (-1) ;
  }

  a = lire_arbre (argv[1]) ;
  b = lire_arbre(argv[2]);

  afficher_arbre (a,0) ;

  /*
  appeler les fonctions que vous
  avez implementees
  */
  printf("-------------------------------\n");
  printf("hauteur arbre nr : %d\n", hauteur_arbre_nr(a));
  printf("hauteur arbre r : %d\n", hauteur_arbre_r(a));

  printf("-------------------------------\n");
  parcourir_arbre_largeur(a);

  printf("-------------------------------\n");
  afficher_nombre_noeuds_par_niveau(a);

  printf("-------------------------------\n");
  printf("Nb Noeuds r : %d\n", nombre_cles_arbre_r(a));
  printf("Nb Noeuds nr : %d\n", nombre_cles_arbre_nr(a));

  printf("-------------------------------\n");
  printf("Trouver cle min : %d\n", trouver_cle_min(a));

  printf("-------------------------------\n");
  printf("Imprimer liste cles triees r : ");
  imprimer_liste_cle_triee_r(a);
  printf("\n");
  imprimer_liste_cle_triee_nr(a);

  printf("-------------------------------\n");
  printf("Arbre Parfait : %s\n",arbre_parfait(a) ? "true" : "false");

  printf("-------------------------------\n");
  printf("Clé supérieure à 9 dans l'arbre a (Affiche -1 si elle n'existe pas) : \n");
  printf("%d\n",rechercher_cle_sup_arbre(a,9) == NULL ? -1 : rechercher_cle_sup_arbre(a,9)->cle);

  printf("-------------------------------\n");
  printf("Clé inférieure à 9 dans l'arbre a (Affiche -1 si elle n'existe pas) : \n");
  printf("%d\n",rechercher_cle_inf_arbre(a,9) == NULL ? -1 : rechercher_cle_inf_arbre(a,9)->cle);

  printf("-------------------------------\n");
  printf("Suppression de la clé 8 : \n");
  afficher_arbre (detruire_cle_arbre(a,8),0) ;

  printf("-------------------------------\n");
  printf("Union a1 et a2 : \n");
  Arbre_t c = union_deux_arbres(a,b);
  imprimer_liste_cle_triee_r(c);
  printf("\n");
  afficher_arbre(c,0);

  printf("-------------------------------\n");
  printf("Intersection a1 et a2 : \n");
  c = intersection_deux_arbres(a,b);
  imprimer_liste_cle_triee_r(c);
  printf("\n");
  afficher_arbre(c,0);
}
