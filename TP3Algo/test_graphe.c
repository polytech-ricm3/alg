#include <stdio.h>
#include <stdlib.h>

#include "graphe.h"

int main (int argc, char **argv)
{
  pgraphe_t g ;
  pchemin_t c;
  p_chemin_temoin_t tem;

  if (argc < 2)
    {
      fprintf (stderr, "erreur parametre \n") ;
      exit (-1) ;
    }

  /*printf("===============TESTS des fonctions de graphe===============\n");*/
  printf("Graphe : \n");
  lire_graphe (argv [1], &g) ;
  ecrire_graphe (g) ;

  printf("\t\t == Caractéristiques du Graphe ==\n");
  printf("\nNombre d'arcs : %d \n", nombre_arcs(g));
  printf("Nombre de sommets : %d \n", nombre_sommets(g));


  printf("\nDegrés sortants des noeuds : \n");
  pnoeud_t n = g;
  for(int i =0; i < nombre_sommets(g);i++)
  {
    printf("\tDegré sortant de %d : %d\n",n->label, degre_sortant_noeud(g,n));
    n = n->noeud_suivant;
  }

  printf("\nDegrés entrants des noeuds : \n");
  n = g;
  for(int i =0; i < nombre_sommets(g);i++)
  {
    printf("\tDegré entrant de %d : %d\n",n->label, degre_entrant_noeud(g,n));
    n = n->noeud_suivant;
  }

  printf("\nDegré maximal du graphe: %d \n", degre_maximal_graphe(g));
  printf("Degré minimal du graphe: %d \n", degre_minimal_graphe(g));

  printf("\nLe graphe est :\n");
  independant(g) == 0? printf("\t- indépendant\n"): printf("\t- pas indépendant\n");
  complet(g) == 0? printf("\t- complet\n"): printf("\t- pas complet\n");
  regulier(g) == 0? printf("\t- régulier\n"): printf("\t- pas régulier\n");

  printf("\nParcours en largeur : \n");
  afficher_graphe_largeur(g);
  printf("Parcours en profondeur : \n");
  afficher_graphe_profondeur(g);

  int origine, destination;
  printf("Saisissez le label d'origine : ");
  scanf("%d",&origine);

  printf("Saisissez le label de destination : ");
  scanf("%d",&destination);
  printf("\nPlus court chemin depuis %d jusqu'à %d : \n",origine,destination);
  int chemin[nombre_sommets(g)+1];
  int nb_n = 0;
  printf("\tDistance : %d\n",plus_court_chemin(g,origine,destination,chemin,&nb_n));
  printf("\tNombre de noeuds du chemin : %d\n", nb_n);
  printf("\tNoeuds à parcourir : ");
  for(int i = nb_n-1; i >= 0; --i)
    printf("%d ",chemin[i]);
  printf("\n");

  printf("Distance entre %d et %d : %d\n",origine,destination,distance(g,chercher_noeud(g,origine),chercher_noeud(g,destination)));

  n = g;
  while (n != NULL)
  {
    printf("Excentricite de %d : %d\n",n->label,excentricite(g,n));
    n = n->noeud_suivant;
  }

  printf("Diametre de g : %d\n",diametre(g));

  printf("=====================EXAMEN 2017=============================\n");

  printf("***\tTEST HAMILTONIEN\t***\n\n");
  if(!c)
  {
    fprintf (stderr,"Le chemin n'a pas ete passe en parametre.\n");
    exit (-1);
  }
  printf("Realite : %i\n",tem->est_hamiltonien);
  printf("D'apres la fonction : %i\n\n",hamiltonien(g,c));

  /*printf("***\tTEST SIMPLE\t***\n\n");
simple(g,c);
  printf("Simple ? %d\n\n",simple(g,c));*/

}
