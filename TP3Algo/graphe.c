/*
Structures de type graphe
Structures de donnees de type liste
(Pas de contrainte sur le nombre de noeuds des  graphes)
*/


#include <stdio.h>
#include <stdlib.h>

#include <limits.h>
#include "graphe.h"
#include "pile.h"
#include "file.h"
#include "tas.h"

void empiler_noeud_graph(ppile_t pile,pgraphe_t a_empiler,pgraphe_t ref)
{
  while(a_empiler)
  {
    if(a_empiler != ref)
    empiler(pile,a_empiler);
    a_empiler = a_empiler->noeud_suivant;
  }
}

pnoeud_t chercher_noeud (pgraphe_t g, int label)
{
  pnoeud_t p ;

  p = g ;

  while ((p!= NULL) && (p->label != label))
  {
    p = p->noeud_suivant ;
  }
  return p ;
}

parc_t existence_arc (parc_t l, pnoeud_t n)
{
  parc_t p = l ;

  while (p != NULL)
  {
    if (p->noeud == n)
    return p ;
    p = p->arc_suivant ;
  }
  return p ;

}

void ajouter_arc (pnoeud_t o, pnoeud_t d, int distance)
{
  parc_t parc ;

  parc = (parc_t) malloc (sizeof(arc_t)) ;

  if (existence_arc (o->liste_arcs, d) != NULL)
  {
    fprintf(stderr, "ajout d'un arc deja existant\n") ;
    exit (-1) ;
  }

  parc->poids = distance ;
  parc->noeud = d ;
  parc->arc_suivant = o->liste_arcs ;
  o->liste_arcs = parc ;
  return ;
}

void lire_graphe (char * file_name, pgraphe_t *g)
{
  pnoeud_t p = NULL ; /* premier noeud du graphe */
  pnoeud_t l = NULL ; /* dernier noeud du graphe */
  FILE *f ;
  int nb_nodes ;
  int i ;
  pnoeud_t c ;
  int origine, destination, distance ;

  printf ("fichier %s\n", file_name) ;
  f = fopen (file_name, "r") ;
  if (f == NULL)
  {
    fprintf (stderr,"Le fichier %s n'existe pas\n", file_name) ;
    exit (-1) ;
  }

  fscanf (f, "%d", &nb_nodes) ;

  for (i = 0 ; i < nb_nodes; i++)
  {
    c = (pnoeud_t) malloc (sizeof (noeud_t)) ;
    c->noeud_suivant = NULL ;
    c->couleur = 0;

    if (fscanf (f, "%d", &c->label) == EOF)
    {
      fprintf (stderr, "erreur dans le contenu du fichier label %d\n", c->label) ;
      exit (-1) ;
    }

    if (chercher_noeud (p, c->label) !=NULL)
    {
      fprintf (stderr, "erreur label deja dans le graphe %d\n",c->label) ;
      exit (-1) ;
    }

    if (p == NULL)
    {
      p = c ;
      l = c ;
    }
    else
    {
      l->noeud_suivant = c ;
      l = c ;
    }
  }

  while (fscanf (f, "%d %d %d", &origine, &destination, &distance) != EOF)
  {
    pnoeud_t o, d ;

    o = chercher_noeud (p, origine) ;
    if (o == NULL)
    {
      fprintf (stderr, "erreur noeud origine %d\n", origine) ;
      exit (-1) ;
    }
    d = chercher_noeud (p, destination) ;
    if (d == NULL)
    {
      fprintf (stderr, "erreur noeud destination %d\n", destination) ;
      exit (-1) ;
    }

    ajouter_arc (o, d, distance) ;

  }


  *g = p ;
}

void ecrire_graphe (pnoeud_t p)
{

  parc_t arc ;

  while (p != NULL)
  {
    printf ("Noeud %d [C %i]: ", p->label,p->couleur) ;
    arc = p->liste_arcs ;
    while ( arc != NULL)
    {
      printf (" (%d %d) ", arc->noeud->label, arc->poids ) ;
      arc = arc->arc_suivant ;
    }
    printf ("\n") ;
    p = p->noeud_suivant ;
  }

  return ;
}

// ===================================================================


int nombre_arcs (pgraphe_t g)
{
  /*
  cette fonction renvoie le nombre d'arcs du graphe g
  */
  int nb = 0;
  parc_t p = g->liste_arcs;

  if (g == NULL)
  return 0;
  while (g != NULL)
  {
    while (p != NULL)
    {
      nb = nb + 1;
      p = p->arc_suivant;
    }
    g = g->noeud_suivant;
  }
  return nb ;
}

int nombre_sommets (pgraphe_t g)
{
  if(g == NULL)
  return 0;
  return nombre_sommets(g->noeud_suivant) + 1;
}


int degre_sortant_noeud (pgraphe_t g, pnoeud_t n)
{
  if(!g || !n) return 0;
  pnoeud_t node = g;
  while(node != NULL && node->label != n->label)
  node = node->noeud_suivant;
  if(node != NULL)
  return nombre_arcs(node);
  else
  return 0;
}

int degre_entrant_noeud (pgraphe_t g, pnoeud_t n)
{
  /*
  Cette fonction retourne le nombre d'arcs entrants
  dans le noeud n dans le graphe g
  */
  if(!g || !n)
  return 0;

  int nb = 0;
  pnoeud_t node = g;
  parc_t arc;

  while(node != NULL)
  {
    arc = node->liste_arcs;
    while(arc != NULL)
    {
      if(arc->noeud->label == n->label)
      nb++;
      arc = arc->arc_suivant;
    }
    node = node->noeud_suivant;
  }
  return nb;
}

int degre_maximal_graphe (pgraphe_t g)
{
  /*
  Max des degres des noeuds du graphe g
  */
  pnoeud_t n = g;
  int max = degre_sortant_noeud(g,n);
  n = n->noeud_suivant;
  while (n != NULL)
  {
    max = max(max,degre_sortant_noeud(g,n));
    n = n->noeud_suivant;
  }

  return max;
}


int degre_minimal_graphe (pgraphe_t g)
{
  /*
  Min des degres des noeuds du graphe g
  */
  if(g == NULL)
  return 0;

  pnoeud_t n = g;
  int min = degre_sortant_noeud(g,n);
  n = n->noeud_suivant;
  while(n != NULL)
  {
    min = min(min,degre_sortant_noeud(g,n));
    n = n->noeud_suivant;
  }


  return min ;
}


int independant (pgraphe_t g)
{
  /* Les aretes du graphe n'ont pas de sommet en commun */
  if (g ==  NULL)
  return 0 ;
  pnoeud_t n = g;
  while (n != NULL && degre_entrant_noeud(g,n) + degre_sortant_noeud(g,n) <= 1)
  {
    n = n->noeud_suivant;
  }
  return n == NULL;
}

int complet (pgraphe_t g)
{
  if(!g) return 0;
  pgraphe_t tmp = g;
  pgraphe_t reference_avancement = g;
  ppile_t pile = creer_pile();
  empiler_noeud_graph(pile,tmp,reference_avancement);
  tmp = g;
  int res = 1;
  while(reference_avancement && res)
  {
    res = existence_arc(reference_avancement->liste_arcs,depiler(pile)) ? 1 : 0;
    reference_avancement = reference_avancement->noeud_suivant;
    empiler_noeud_graph(pile,tmp,reference_avancement);
    tmp = g;
  }
  return res;
}

int regulier (pgraphe_t g)
{
  return (degre_maximal_graphe(g) == degre_minimal_graphe(g));
}

void afficher_graphe_profondeur (pgraphe_t g)
{
  /*
  afficher les noeuds du graphe avec un parcours en profondeur
  */

  if (g == NULL)
  return;

  //Initialisation des visistes
  int nb_sommets = nombre_sommets(g)+1;
  int visites[nb_sommets];
  for(int i = 0; i < nb_sommets; i++)
  visites[i] = 0;

  ppile_t p = creer_pile();
  empiler(p,g);

  pnoeud_t n, h;
  while(!pile_vide(p))
  {
    n = depiler(p);

    if (visites[n->label] == 0)
    {
      visites[n->label] = 1;
      printf("%d ", n->label);
      parc_t t = n->liste_arcs;
      while (t != NULL)
      {
        h = t->noeud;
        empiler(p,h);
        t = t->arc_suivant;
      }

    }
  }
  printf("\n");
  return;

}

void colorier_graphe (pgraphe_t g)
{
  /*
  coloriage du graphe g
  Les couleurs des noeuds du graphe sont dans le tableau couleurs
  */
  /// A Revoir !!
  if(!g) return ;
  g->couleur = 0;
  while(g)
  {
    parc_t a = g->liste_arcs;
    while(a)
    {
      a->noeud->couleur = g->couleur + 1;
      a = a->arc_suivant;
    }
    g = g->noeud_suivant;
  }
}

void afficher_graphe_largeur (pgraphe_t g)
{
  /*
  afficher les noeuds du graphe avec un parcours en largeur
  */
  if(g == NULL)
  return;

  //Initialisation du tableau des visites
  int nb_sommets = nombre_sommets(g)+1;
  int visites[nb_sommets];
  for(int i = 0; i < nb_sommets; i++)
  visites[i] = 0;

  //Initialisation File
  pfile_t f = creer_file();
  deposer_file(f,g);


  pnoeud_t p, t;
  while(!file_vide(f))
  {
    p = retirer_file(f);
    if(visites[p->label] == 0)
    {
      printf("%d ", p->label);
      visites[p->label] = 1;
      t = p;
      while(t != NULL)
      {
        if(visites[t->label] == 0)
        {
          deposer_file(f,t);
        }
        t = t->noeud_suivant;
      }
    }
  }
  printf("\n");
  return ;
}

/*** Initialisation du grahe pour l'algo de Dijkstra **/
void Init(pgraphe_t g, int s)
{
  pnoeud_t n = g;

  while (n != NULL)
  {
    n->d = INT_MAX;
    n->pi = NULL;
    n = n->noeud_suivant;
  }
  chercher_noeud(g,s)->d = 0;
}

void Relax(pnoeud_t q,pnoeud_t r)
{
  parc_t arcqr = existence_arc(q->liste_arcs,r);
  if(arcqr != NULL)
  {
    if(q->d + arcqr->poids < r->d)
    {
      r->d = q->d + arcqr->poids;
      r->pi = q;
    }
  }
}

int plus_court_chemin (pgraphe_t g, int origine, int destination, int chemin[], int *nb_noeuds)
{
  /*
  Calcul de la longueur du plus court chemin
  entre origine et destination
  La variable chemin contient les noeuds du chemin le plus court
  nb_noeuds indique le nombre de noeuds du chemin
  */
  if(g == NULL || chercher_noeud(g,origine) == NULL || chercher_noeud(g,destination) == NULL)
  return -1;
  ptas_t t = creer_tas();
  pnoeud_t node;

  Init(g,origine);

  node = g;
  while(node != NULL)
  {
    deposer_tas(t,node);
    node = node->noeud_suivant;
  }

  while(!tas_vide(t))
  {
    node = retirer_tas(t);
    //  printf("Node : %d\n", (node == NULL) ? -1 : node->label);
    parc_t arc = node->liste_arcs;
    while(arc != NULL)
    {
      //    printf("\tArc vers : %d\n", arc->noeud->label);
      Relax(node,arc->noeud);
      arc = arc->arc_suivant;
    }
  }

  pnoeud_t c = chercher_noeud(g,destination);

  int nb = 0;
  while(c != NULL)
  {
    //  printf("Valeur de Nb : %d\n",nb);
    //  printf("Noeud %d | pi : %d\n",c->label, c->pi ? c->pi->label : 0);
    chemin[nb++] = c->label;
    c = c->pi;
  }

  *nb_noeuds = nb;
  return chercher_noeud(g,destination)->d;
}

// ===================================================================

/*
Fonctions de l'examen 2017
*/
int elementaire (pgraphe_t g, pchemin_t c)
{
  //Renvoie 1 si le chemin est élémentaire = ne passe pas deux fois par le meme noeud
  if (g == NULL)
  return 0;

  int nb = nombre_sommets(g) + 1;
  int tmp[nb];
  for (int i = 0; i < nb; i++)
  tmp[i] = 0;

  int n = 0;
  while(c->next != NULL)
  {
    n =  c->current->label;
    if (tmp[n] == 1)
    return 0;
    tmp[n] = 1;
    c = c->next;
  }
  return 1;
}

pid_arc_t codearc (pnoeud_t a, pnoeud_t n)
{
  pid_arc_t arc = malloc(sizeof(pid_arc_t));
  arc->src = n->label;
  arc->dst = a->label;
  return arc;
}

int simple (pgraphe_t g, pchemin_t c)
{
  //Renvoie 1 si le chemin est simple = ne passe pas deux fois par le meme arc
  if (g == NULL)
  return 0;
  //initialisation tableau
  int nb = nombre_arcs(g);
  pid_arc_t tab[nb];
  for (int i = 0; i < nb; i++)
  {
    tab[i]->src = 0;
    tab[i]->dst = 0;
  }
  //stockage des arcs dans tableau
  int k = 0;
  while(c->next != NULL)
  {
    printf("noeud: %d, noeud= %d\n",c->current->label, c->next->current->label);
    tab[k] = codearc(c->current, c->next->current);
    c = c->next;
    k++;
  }
  printf("blabla\n");
  //vérification
  for (int i = 0; i < nb-1; i++)
  {
    for (int j = i + 1; j < nb; j++)
    {
      if (tab[i]->src == tab[j]->src && tab[i]->dst == tab[j]->dst)
      return 0;
    }
  }
  return 1;
}

int eulerien (pgraphe_t g, pchemin_t c)
{
  //Renvoie 1 si le chemin est eulerien = toutes les arretes du graphe sont utilisées dans le chemin
  if(!g || !c) return 0;
  ppile_arc_t pile_arcs = creer_pile_arc();
  while(g)
  {
    parc_t a = g->liste_arcs;
    while(a)
    {
      pid_arc_t arc = codearc(g,a->noeud);
      if(!arc_est_dans_pile(pile_arcs, arc))
      empiler_arc(pile_arcs,arc);
      a = a->arc_suivant;
    }
    g = g->noeud_suivant;
  }
  pid_arc_t arc = depiler_arc(pile_arcs);
  while(c && !pile_vide_arc(pile_arcs) && arc_est_dans_chemin(c,arc))
  {
    arc = depiler_arc(pile_arcs);
  }
  return pile_vide_arc(pile_arcs);
}


int hamiltonien (pgraphe_t g, pchemin_t c)
{
  //Renvoie 1 si le chemin est hamiltonien = tous les noeuds du graphe sont utilisés dans le chemin
  if(!g || !c) return 0;
  ppile_t pile = creer_pile();
  while(g)
  {
    empiler(pile,g);
    g = g->noeud_suivant;
  }
  pnoeud_t tmp = depiler(pile);
  while(c && !pile_vide(pile) && est_dans_chemin(c,tmp))
  {
    tmp = depiler(pile);
  }
  return pile_vide(pile);
}

int graphe_eulerien (pgraphe_t g)
{
  //Renvoie 1 si le graphe est eulerien = si graphe contient au moins un chemin eulerien
  return 0;
}

int graphe_hamiltonien (pgraphe_t g)
{
  //Renvoie 1 si le graphe est hamiltonien = si graphe contient au moins un chemin hamiltonien
  return 0;
}

int distance (pgraphe_t g, pnoeud_t x, pnoeud_t y)
{
  //Calcul la distance entre x et y, distance = longueur du plus court chemin entre x et y
  if(g == NULL || x == NULL || y == NULL)
  return -1;
  int chemin[nombre_sommets(g)];
  int nb_noeuds;

  return plus_court_chemin (g, x->label, y->label, chemin, &nb_noeuds);
}

int excentricite (pgraphe_t g, pnoeud_t n)
{
  //Calcul de l'excentricité du noeud n dans le graphe g, excentricité d'un noeud = sa distance maximale avec les autre noeuds du graphe
  if(g == NULL || n == NULL)
  return -1;
  n->excentricite = 0;

  int nb_sommets = nombre_sommets(g)+1;
  int visites[nb_sommets];
  for(int i = 0; i < nb_sommets; i++)
  visites[i] = 0;

  //Initialisation File
  pfile_t f = creer_file();
  deposer_file(f,g);


  pnoeud_t p, t;
  while(!file_vide(f))
  {
    p = retirer_file(f);
    if(visites[p->label] == 0)
    {
      n->excentricite = max(n->excentricite,distance(g,n,p));
      visites[p->label] = 1;
      t = p;
      while(t != NULL)
      {
        if(visites[t->label] == 0)
        {
          deposer_file(f,t);
        }
        t = t->noeud_suivant;
      }
    }
  }
  return n->excentricite;
}

int diametre (pgraphe_t g)
{
  pnoeud_t n = g;

  while (n != NULL)
  {
    excentricite(g,n);
    n = n->noeud_suivant;
  }
  n = g;
  int diametre = 0;
  while (n != NULL)
  {
    diametre = max(diametre,n->excentricite);
    n = n->noeud_suivant;
  }
  return diametre;
}
