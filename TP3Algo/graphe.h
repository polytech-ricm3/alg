#ifndef GRAPHE_H
#define GRAPHE_H

#define max(a,b) ((a)>(b)?(a):(b))
#define min(a,b) ((a)<(b)?(a):(b))
typedef struct a *parc_t ;

typedef struct{
    unsigned int est_hamiltonien:1;
    unsigned int est_eulerien:1;
    unsigned int est_simple:1;
}temoin_t, *p_chemin_temoin_t;

/*
definition des types noeud et pointeur de noeud
un graphe est constitué d'une liste de noeuds
*/

typedef struct n
{
  int        label ; // label du noeud/sommet
  int        couleur; //couleur du noeud
  parc_t     liste_arcs ; // arcs sortants du noeud
  int        d;
  int        excentricite;
  struct n   *pi;
  struct n   *noeud_suivant ; // noeud/sommet suivant du graphe
} noeud_t, *pnoeud_t ;
/*
definition des types arc et pointeur d'arc
Les arcs sortant d'un noeud sont chainés
Pour chaque arc, il y a un poids qui peut par exemple correspondre
a une distance
*/

typedef struct a {
  int         poids ; // poids de l arc
  pnoeud_t    noeud ; // identificateur du noeud destinataire
  struct a *  arc_suivant ; // arc suivant
} arc_t, *parc_t ;
/*
pgraphe_t: pointeur vers le premier noeud d'un graphe
*/

typedef pnoeud_t pgraphe_t ;



/*
DEFINIR LE TYPE chemin_t (examen 2017)
*/
typedef struct c {
  pnoeud_t current;
  int longueur;
  struct c *prec;
  struct c *next;
}chemin_t, *pchemin_t;



typedef struct {
  int src;
  int dst;
}id_arc_t, *pid_arc_t;

void lire_graphe (char * file_name, pgraphe_t *g) ;

pnoeud_t chercher_noeud (pgraphe_t g, int label);

void ecrire_graphe (pnoeud_t p) ;

int nombre_arcs (pgraphe_t g) ;

int nombre_sommets (pgraphe_t g) ;

int degre_sortant_noeud (pgraphe_t g, pnoeud_t n) ;

int degre_entrant_noeud (pgraphe_t g, pnoeud_t n) ;

int degre_maximal_graphe (pgraphe_t g) ;

int degre_minimal_graphe (pgraphe_t g) ;

int independant (pgraphe_t g) ;

int complet (pgraphe_t g) ;

int regulier (pgraphe_t g) ;

void afficher_graphe_profondeur (pgraphe_t g) ;

void colorier_graphe (pgraphe_t g) ;

void afficher_graphe_largeur (pgraphe_t g);

int plus_court_chemin (pgraphe_t g, int origine, int destination, int chemin[],
  int *nb_noeuds) ;

// ===================================================================

  /*
Fonctions de l'examen 2017
  */
int elementaire (pgraphe_t g, pchemin_t c);

int simple (pgraphe_t g, pchemin_t c);

int eulerien (pgraphe_t g, pchemin_t c);

int hamiltonien (pgraphe_t g, pchemin_t c);

int graphe_eulerien (pgraphe_t g);

int graphe_hamiltonien (pgraphe_t g);

int distance (pgraphe_t g, pnoeud_t x, pnoeud_t y);

int excentricite (pgraphe_t g, pnoeud_t n);

int diametre (pgraphe_t g);

parc_t existence_arc (parc_t l, pnoeud_t n);


//Fonctions utiles
int est_dans_chemin(pchemin_t c, pnoeud_t n);
int arc_est_dans_chemin(pchemin_t c, pid_arc_t a);
pnoeud_t chercher_noeud_label(pgraphe_t g,int label);
<<<<<<< HEAD
void afficher_chemin(pchemin_t c);
void generate_chemin(char * file_name, pchemin_t *c, pgraphe_t g);
=======
void afficher_chemin(pchemin_t c,p_chemin_temoin_t t);
void generate_chemin(char * file_name, pchemin_t *c, p_chemin_temoin_t *temoin, pgraphe_t g);
int arc_est_dans_chemin(pchemin_t c, pid_arc_t a);



>>>>>>> thomas

#endif
