#include "graphe.h"
#define MAX_SIZE 200

typedef struct {
  int nb_elem;
  pnoeud_t Tab [MAX_SIZE];
} tas_t, *ptas_t ;

ptas_t creer_tas();

int tas_vide (ptas_t t);

int deposer_tas (ptas_t t, pnoeud_t n);

pnoeud_t retirer_tas (ptas_t t);
