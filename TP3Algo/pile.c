#include <stdlib.h>
#include "pile.h"

ppile_t creer_pile ()
{
  ppile_t p = malloc(sizeof(pile_t));
  p->sommet = 0;
  return p;
}

int pile_vide (ppile_t p)
{
  return p->sommet <= 0;
}

int pile_pleine (ppile_t p)
{
  return p->sommet >= MAX_PILE_SIZE;
}

pnoeud_t depiler (ppile_t p)
{
  pnoeud_t n = p->Tab[p->sommet];
  p->Tab[p->sommet] = NULL;
  p->sommet--;
  return n;
}

int empiler (ppile_t p, pnoeud_t pn)
{
  if(pn == NULL)
    return 0;
  p->sommet++;
  p->Tab[p->sommet] = pn;
  return 1;
}

int est_dans_pile(ppile_t p, pnoeud_t n)
{
  if(!n || !p) return 0;
  int res = 0;
  for(int i = 0; i < p->sommet && !res; i++)
  {
    res = (p->Tab[i] == n);
  }
  return res;
}

ppile_arc_t creer_pile_arc ()
{
  ppile_arc_t p = malloc(sizeof(pile_arc_t));
  p->sommet = 0;
  return p;
}
pid_arc_t depiler_arc (ppile_arc_t p)
{
  pid_arc_t n = p->Tab[p->sommet];
  p->Tab[p->sommet] = NULL;
  p->sommet--;
  return n;
}
int empiler_arc (ppile_arc_t p, pid_arc_t pn)
{
  if(!pn) return 0;
  p->sommet++;
  p->Tab[p->sommet] = pn;
  return 1;
}
int arc_est_dans_pile(ppile_arc_t p, pid_arc_t n)
{
  if(!n || !p) return 0;
  int res = 0;
  for(int i = 0; i < p->sommet && !res; i++)
  {
    res = (p->Tab[i]->src == n->src && p->Tab[i]->dst == n->dst);
  }
  return res;
}