#include <stdlib.h>
#include "file.h"

pfile_t creer_file ()
{
  pfile_t f = malloc(sizeof(file_t));
  f->tete = 0;
  f->queue = 0;
  f->nb_elem = 0;
  return f;
}

int file_vide (pfile_t f)
{
  return f->nb_elem == 0;
}

int file_pleine (pfile_t f)
{
  return f->nb_elem == MAX_FILE_SIZE;
}

pnoeud_t retirer_file (pfile_t f)
{
  pnoeud_t n = f->Tab[f->tete];
  f->Tab[f->tete] = NULL;
  f->tete = (f->tete+1) % MAX_FILE_SIZE;
  f->nb_elem--;
  return n;
}

int deposer_file (pfile_t f, pnoeud_t p)
{
  if(p == NULL)
    return 0;
  f->Tab[f->queue] = p;
  f->queue = (f->queue + 1) % MAX_FILE_SIZE;
  f->nb_elem++;
  return 1;
}

pnoeud_t afficher_sommet(pfile_t f)
{
  pnoeud_t n = f->Tab[f->tete];
  return n;
}
