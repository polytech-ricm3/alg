#include "graphe.h"

#define MAX_PILE_SIZE       32

typedef struct {
  int sommet ;
  pnoeud_t Tab [MAX_PILE_SIZE] ;
} pile_t, *ppile_t ;

typedef struct {
  int sommet ;
  pid_arc_t Tab [MAX_PILE_SIZE] ;
} pile_arc_t, *ppile_arc_t ;

ppile_t creer_pile () ;
int pile_vide (ppile_t p) ;
int pile_pleine (ppile_t p) ;
pnoeud_t depiler (ppile_t p)  ;
int empiler (ppile_t p, pnoeud_t pn) ;
int est_dans_pile(ppile_t p, pnoeud_t n);

ppile_arc_t creer_pile_arc () ;
static inline int pile_vide_arc (ppile_arc_t p) {return p->sommet <= 0;}
static inline int pile_pleine_arc (ppile_arc_t p) {return p->sommet >= MAX_PILE_SIZE;}
pid_arc_t depiler_arc (ppile_arc_t p)  ;
int empiler_arc (ppile_arc_t p, pid_arc_t pn) ;
int arc_est_dans_pile(ppile_arc_t p, pid_arc_t n);
