#include "tas.h"
#include <stdlib.h>


ptas_t creer_tas()
{
  ptas_t t = malloc(sizeof(tas_t));
  t->nb_elem = 0;
  return t;
}

int tas_vide (ptas_t t)
{
  return t->nb_elem <= 0;
}



int deposer_tas (ptas_t t, pnoeud_t n)
{
    if(n == NULL)
      return 0;

    pnoeud_t e = n;
    int i = t->nb_elem + 1;
    t->Tab[i] = e;
    t->nb_elem++;

    return 1;
}

pnoeud_t min_tas(ptas_t t, int* min_index)
{
  pnoeud_t min = t->Tab[1];
  *min_index = 1;
  for(int i = 1; i <= t->nb_elem; i++)
  {
    if(min->d > t->Tab[i]->d)
    {
      *min_index = i;
      min = t->Tab[i];
    }
  }
  return min;
}

pnoeud_t retirer_tas (ptas_t t)
{
    if (t == NULL || t->nb_elem == 0)
    {
        return NULL;
    }

    int min_index;
    pnoeud_t e  = min_tas(t,&min_index);

    int i = min_index;
    while (i != t->nb_elem+1)
    {
        t->Tab[i] = t->Tab[i+1];
        i++;
    }
    t->nb_elem--;
    return e;
}
