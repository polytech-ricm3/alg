#include <stdio.h>
#include <stdlib.h>

#include "graphe.h"

int est_dans_chemin(pchemin_t c, pnoeud_t n)
{
    if(!c || !n) return 0;
    pchemin_t point_depart = NULL;
    int est_dans_chemin = 0;
    while(c && c != point_depart && !est_dans_chemin)
    {
        if(!point_depart)
            point_depart = c;
        est_dans_chemin = (c->current == n);
        c = c->next;
    }
    return est_dans_chemin;
}

int arc_est_dans_chemin(pchemin_t c, pid_arc_t a)
{
    if(!c || !a) return 0;
    pchemin_t point_depart = NULL;
    int est_dans_chemin = 0;
    while(c && c->longueur >= 0 && !est_dans_chemin)
    {
         if(!point_depart)
            point_depart = c;
        if(c->next)
        {
            int src = c->current->label;
            int dst = c->next->current->label;
            est_dans_chemin = (a->src == src && a->dst == dst);
            c = c->next;
        }
    }
    return est_dans_chemin;
}

pnoeud_t chercher_noeud_label(pgraphe_t g,int label)
{
    if(!g) return NULL;
    if(g->label == label) return g;
    return chercher_noeud_label(g->noeud_suivant,label);
}

void generate_chemin(char * file_name, pchemin_t *c, p_chemin_temoin_t *temoin, pgraphe_t g)
{
    FILE *f = fopen(file_name,"r");
    printf("Fichier de chemin : %s\n",file_name);
    if(!f)
    {
        fprintf (stderr,"Le fichier %s n'existe pas\n", file_name) ;
        exit (-1) ;
    }
    p_chemin_temoin_t tmp_temoin = malloc(sizeof(temoin_t));
    int hamiltonient, eulerien;
    fscanf(f,"%d %d",&hamiltonient, &eulerien);
    tmp_temoin->est_hamiltonien = hamiltonient;
    tmp_temoin->est_eulerien    = eulerien;
    
    pchemin_t head = NULL, prec = NULL;
    int label,longueur = 0;

    while(!feof(f) && longueur >= 0)
    {
        head = malloc(sizeof(chemin_t));
        head->next            = NULL;
        head->prec            = prec;
        head->current         = NULL;

        if(head->prec)
            head->prec->next = head;

        fscanf(f,"%d %d",&label,&longueur);
        head->longueur = longueur;
        if((head->current = chercher_noeud_label(g,label)) == NULL)
        {
            fprintf (stderr,"Le noeud %d n'existe pas.\n", label) ;
            exit (-1) ;
        }
        if(prec)
        {
            if(!existence_arc(prec->current->liste_arcs, head->current))
            {
                fprintf (stderr,"Chemin impossible.\n") ;
                exit (-1) ;
            }
        }
        prec = head;
        head = head->next;
    }
    fclose(f);
    
    while(prec)
    {
        head = prec;
        prec = head->prec;
    }
    *c      = head;
    *temoin = tmp_temoin;
}

void afficher_chemin(pchemin_t c,p_chemin_temoin_t t)
{
    if(!c || !t) return;
    printf("\n");
    printf("------------------------------------------\n");
    printf("|                 TEMOINS                |\n");
    printf("------------------------------------------\n");
    printf("Hamiltonien : %i                        \n",t->est_hamiltonien);
    printf("Eulerien    : %i                        \n",t->est_eulerien);
    printf("------------------------------------------\n");
    printf("|                 CHEMIN                 |\n");
    printf("------------------------------------------\n");
    
    while(c && c->next)
    {
        pnoeud_t noeud         = c->current;
        pnoeud_t noeud_suivant = c->next->current;
        printf("Noeud %i -> Noeud %i : %i\n",noeud->label,noeud_suivant->label,c->longueur);
        c = c->next;
    }
    printf("------------------------------------------\n");
    printf("\n");
}