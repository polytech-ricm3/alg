#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "a234.h"

void Afficher_Cles_Largeur (Arbre234 a) {
  if(a == NULL)
  return;
  printf("Affichage Cles Largeur : \n");
  for(int i = 1; i <= hauteur(a); i++)
  {
   printf("\tNiveau %d : ", i );
    Afficher_Cles_Niveau(a,i);
    printf("\n");
  }
  printf("\n");
}

void Afficher_Cles_Niveau(Arbre234 a, int niveau)
{
  if (a == NULL || a->t == 0)
  return;

  if(niveau == 1) {
    if(a->t == 2) {
      printf("%d ", a->cles[1]);
    }
    else
    {
      for(int i = 0; i < a->t-1; i++) {
        printf("%d ", a->cles[i]);
      }
    }
  }
  else if(niveau > 1)
  {
    if(a->t == 2)
    {
      Afficher_Cles_Niveau(a->fils[1],niveau-1);
      Afficher_Cles_Niveau(a->fils[2],niveau-1);
    }
    if(a->t == 3)
    {
      Afficher_Cles_Niveau(a->fils[0],niveau-1);
      Afficher_Cles_Niveau(a->fils[1],niveau-1);
      Afficher_Cles_Niveau(a->fils[2],niveau-1);
    }
    if(a->t == 4)
    {
      Afficher_Cles_Niveau(a->fils[0],niveau-1);
      Afficher_Cles_Niveau(a->fils[1],niveau-1);
      Afficher_Cles_Niveau(a->fils[2],niveau-1);
      Afficher_Cles_Niveau(a->fils[3],niveau-1);
    }
  }
}
