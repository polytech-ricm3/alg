#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "a234.h"

int hauteur (Arbre234 a)
{
  int h0, h1, h2, h3 ;

  if (a == NULL)
  return 0 ;

  if (a->t == 0)
  return 0 ;

  h0 = hauteur (a->fils [0]) ;
  h1 = hauteur (a->fils [1]) ;
  h2 = hauteur (a->fils [2]) ;
  h3 = hauteur (a->fils [3]) ;

  return 1 + max (max (h0,h1),max (h2,h3)) ;
}
