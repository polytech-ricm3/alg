#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//#include "a234.h"
#include "file.h"

void Affichage_Cles_Triees_Recursive (Arbre234 a)
{
  if (a == NULL || a->t == 0)
  return;

  if(a->t == 2)
  {
    Affichage_Cles_Triees_Recursive(a->fils[1]);
    printf("%d ", a->cles[1]);
    Affichage_Cles_Triees_Recursive(a->fils[2]);
  }
  else
  {
    Affichage_Cles_Triees_Recursive(a->fils[0]);
    printf("%d ", a->cles[0]);
    Affichage_Cles_Triees_Recursive(a->fils[1]);
    printf("%d ", a->cles[1]);
    Affichage_Cles_Triees_Recursive(a->fils[2]);
    if(a->t == 4)
    {
      printf("%d ", a->cles[2]);
      Affichage_Cles_Triees_Recursive(a->fils[3]);
    }
  }

}


//Fonction à utilsier avec qsort pour trier en ordre croissant
static int compare (void const *a, void const *b)
{
  int const *pa = a;
  int const *pb = b;

  return *pa - *pb;
}


void Affichage_Cles_Triees_NonRecursive (Arbre234 a)
{
  if(a == NULL)
  return;
  pfile_t f = creer_file();
  ajouter_arbre_file(f,a,1); //On met l'arbre dans une file
  int cles[NombreCles(a)];
  Arbre234 n;
  int index = 0;

  while(!file_vide(f)) //On parcours la file
  {
    n = retirer_file(f);

    if(n->t == 2) //On ajoute ses cles dans un tableau
    {
      cles[index++] = n->cles[1];
    }
    else if(n->t == 3 || n->t == 4)
    {
      cles[index++] = n->cles[0];
      cles[index++] = n->cles[1];
      if(n->t == 4)
        cles[index++] = n->cles[2];
    }
  }

  qsort(cles,index,sizeof(int),compare); //On trie le tableau

  for(int i = 0; i < index; i++)
  {
    printf("%d ",cles[i]); //On affiche
  }
  printf("\n");
}
