#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "a234.h"

int somme_noeud(Arbre234 a)
{
  if(a == NULL)
    return 0;
  int sum = 0;
  switch(a->t)
  {
    case 4:
    sum += a->cles[2] + a->cles[1] + a->cles[0];
    break;
    case 3:
    sum += a->cles[1] + a->cles[0];
    break;
    case 2:
    sum += a->cles[1];
    break;
  }
  return sum;
}

Arbre234 max_somme_noeud(Arbre234 a1, Arbre234 a2)
{
  return (somme_noeud(a1) >= somme_noeud(a2)) ? a1 : a2;
}

Arbre234 noeud_max (Arbre234 a)
{
  Arbre234 a0 = NULL,a1 = NULL,a2 = NULL,a3 = NULL;

  if (a == NULL || a->t == 0)
    return NULL;


  if(a->t == 2)
  {
    a1 = noeud_max(a->fils[1]);
    a2 = noeud_max(a->fils[2]);
  }
  else
  {
    a0 = noeud_max(a->fils[0]);
    a1 = noeud_max(a->fils[1]);
    a2 = noeud_max(a->fils[2]);
    if(a->t == 4)
      a3 = noeud_max(a->fils[3]);
  }

  Arbre234 max = max_somme_noeud(a, max_somme_noeud(a0, max_somme_noeud(a1, max_somme_noeud(a2,a3))));
  printf("Fcle : %d\n",(max != NULL) ?max->cles[1] : -5);
  return max;
}
