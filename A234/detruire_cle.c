#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "a234.h"
#include "file.h"
void Detruire_Cle (Arbre234 *a, int cle)
{
  if(a==NULL || RechercherCle(*a,cle) == NULL){	
		return;
	}

	int c0,c1,c2;
  Arbre234 na = NULL;
	pfile_t f = creer_file();

	ajouter_arbre_file(f,*a,1);

	while(!file_vide(f)){
    Arbre234 n = retirer_file(f);
		c0 = n->cles[0];
		c1 = n->cles[1];
		c2 = n->cles[2];

	   if(n->t ==2)
     {
			if(c1 != cle)
				ajouter_cle(&na, c1, 0, NULL);
		 }
     else if(n->t ==3 || n->t == 4)
     {
       if(c0 != cle)	//Même principe
         ajouter_cle(&na, c0, 0, NULL);
       if(c1 != cle)
         ajouter_cle(&na, c1, 0, NULL);
       if(n->t == 4 && c2 != cle)
         ajouter_cle(&na, c2, 0, NULL);
     }
	}
	*a = na;
/*
  if(a == NULL)
    return;
  *a = RechercherCle(*a,cle); //RechercherCleArbrePere(cle,a,&branche);
  int pos = position(cle,*a);
    if(pos < (*a)->t-1 && (*a)->cles[pos]) //Si il s'agit d'un noeud interne
    {
      if((*a)->t != 2)
      {
        (*a)->cles[pos] = CleMax((*a)->fils[pos]);
        Detruire_Cle(&(*a)->fils[pos],(*a)->cles[pos]);
      }
      else
      {
        if((*a)->fils[pos+1]->t-1 > 1)
        {
          (*a)->cles[pos] = CleMin((*a)->fils[pos+1]);
          Detruire_Cle(&(*a)->fils[pos+1],(*a)->cles[pos]);
        }
        else
        {
          Fusion(*a,pos);
          Detruire_Cle(&(*a)->fils[pos],cle);
        }
      }
    }
    else // Si la cle n'est pas dans le noeud
    {
      if((*a)->fils[pos]->t-1 == 1)
      {
        if(pos > 1 && (*a)->fils[pos-1]->t-1 > 1)
        {
          //rotation_droite(a,i);
        }
        else if (pos <= (*a)->t-1 && (*a)->fils[pos-1]->t-1 > 1)
        {
          //rotation_gauche(a,i);
        }
        else
        {
          if(pos > 1)
          {
            pos--;
          }
          Fusion(*a,pos);
        }
      }
      Detruire_Cle(&(*a)->fils[pos],cle);
    }

    if(pos <= (*a)->t-1 && (*a)->cles[pos] == cle)
    {
      for(int i = pos; i < (*a)->t-1; i++)
      {
        (*a)->cles[i] = (*a)->cles[i+1];
      }
      (*a)->t--;
    }
  */
}

Arbre234 RechercherCleArbrePere(int cle, Arbre234 *pere, int *branche){
  int res_branche = -1;
  int i, pos;
  Arbre234 *tmp = NULL;
  if(pere == NULL)
  {
    *branche = res_branche;
    return NULL;
  }


  /*****************************************************/
  // Cas où la clé est dans le noeud  racine.
  int nb_cles = sizeof((*pere)->cles)/sizeof(int);
  for(i = 0; i < nb_cles; i++)
  {
    if((*pere)->cles[i] == cle)
    {
      tmp = pere;
    }
  }

  /**********************************************/

  if(!tmp)
  {
    for(i = 0; i < (*pere)->t; i++)
    {
      for(int j = 0; j < nb_cles; j++)
      {
        if((*pere)->fils[i] && (*pere)->fils[i]->cles[j] == cle)
        {
          res_branche = i;
          tmp = pere;
        }
      }
    }
    if(!tmp)
    {
      pos = position(cle,*pere);
      return RechercherCleArbrePere(cle,&((*pere)->fils[pos]),branche);
    }
    *branche = res_branche;
    return *tmp;
  }else
  {
    *branche = res_branche;
    return *tmp;
  }
}

void Fusion(Arbre234 arbre,int i){
  Arbre234 G,D;
  G = arbre->fils[i];
  D = arbre->fils[i + 1];
  G->cles[2] = arbre->cles[i];
  G->cles[3] = D->cles[1];
  G->fils[2] = D->fils[1];
  G->fils[3] = D->fils[2];
  G->t = 3;
  for(int j = i; j < arbre->t - 2; j++)
  {
    arbre->cles[j] = arbre->cles[j+1];
    arbre->fils[j + 1] = arbre->fils[j + 2];
  }
  arbre->t--;
}
