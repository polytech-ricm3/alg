#include <stdlib.h>
#include "file.h"

pfile_t creer_file ()
{
  pfile_t f = malloc(sizeof(file_t));
  f->tete = 0;
  f->queue = 0;
  f->nb_elem = 0;
  return f;
}

int file_vide (pfile_t f)
{
  return f->nb_elem == 0;
}

int file_pleine (pfile_t f)
{
  return f->nb_elem == MAX_FILE_SIZE;
}

Arbre234 retirer_file (pfile_t f)
{
  Arbre234 n = f->Tab[f->tete];
  f->Tab[f->tete] = NULL;
  f->tete = (f->tete+1) % MAX_FILE_SIZE;
  f->nb_elem--;
  return n;
}

int deposer_file (pfile_t f, Arbre234 p)
{
  if(p == NULL)
    return 0;
  f->Tab[f->queue] = p;
  f->queue = (f->queue + 1) % MAX_FILE_SIZE;
  f->nb_elem++;
  return 1;
}

Arbre234 afficher_sommet(pfile_t f)
{
  Arbre234 n = f->Tab[f->tete];
  return n;
}

void ajouter_arbre_file(pfile_t f, Arbre234 a, int h)
{
  if(h)	//On distingue le cas de la racine
  deposer_file(f,a);

  switch(a->t){	//On différencie les 4 types d'arbre

  case 2 :
  deposer_file(f,a->fils[1]);
  deposer_file(f,a->fils[2]);
  ajouter_arbre_file(f,a->fils[1],0);
  ajouter_arbre_file(f,a->fils[2],0);
  break;

  case 3 :
  deposer_file(f,a->fils[0]);
  deposer_file(f,a->fils[1]);
  deposer_file(f,a->fils[2]);
  ajouter_arbre_file(f,a->fils[0],0);
  ajouter_arbre_file(f,a->fils[1],0);
  ajouter_arbre_file(f,a->fils[2],0);
  break;

  case 4 :
  deposer_file(f,a->fils[0]);
  deposer_file(f,a->fils[1]);
  deposer_file(f,a->fils[2]);
  deposer_file(f,a->fils[3]);
  ajouter_arbre_file(f,a->fils[0],0);
  ajouter_arbre_file(f,a->fils[1],0);
  ajouter_arbre_file(f,a->fils[2],0);
  ajouter_arbre_file(f,a->fils[3],0);
  break;

  default :	//Si type 0 on ne fait plus rien
  return;
}
}
