#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "a234.h"


void help()
{
  printf("------------------------------------------------------------------------\n");
  printf("                           AIDE\n");
  printf("------------------------------------------------------------------------\n");
  printf("<nom du test (afficherArbre,hauteur,cleMin,cleMax, nbCles, noeudMax\n clesLargeur, clesTriees, detruireCle)>   : Execute le test\n");
  printf("h                                         : Affiche cette aide\n");
  printf("q                                         : Quitter\n");
  printf("------------------------------------------------------------------------\n");
}


int main(int argc, char **argv) {
  Arbre234 a;

  if (argc != 2) {
    fprintf(stderr, "il manque le parametre nom de fichier\n");
    exit(-1);
  }
  char *action = malloc(sizeof(char) * 50);
  a = lire_arbre(argv[1]);

  help();

  while(1)
  {

    printf("\n");
    printf(">> ");
    scanf("%s",action);
    fflush(stdin);
    if(strcmp(action,"q") == 0) exit(0);
    else if(strcmp(action,"h") == 0)
    help();
    else if(strcmp(action, "afficherArbre") == 0)
    {
      printf("==== Afficher arbre ====\n");
      afficher_arbre(a, 0);
    }
    else if(strcmp(action, "hauteur") == 0)
    {
      printf("==== Hauteur arbre ====\n");
      printf("Nombre niveaux: %d\n", hauteur(a));
    }
    else if(strcmp(action, "cleMin") == 0)
    {
      printf("==== Clé Min ====\n");
      printf("CleMin : %d\n", CleMin(a));
    }
    else if(strcmp(action, "cleMax") == 0)
    {
      printf("==== Clé Max ====\n");
      printf("CleMax : %d\n", CleMax(a));
    }
    else if(strcmp(action, "nbCles") == 0)
    {
      printf("==== Nombre Clés ====\n");
      printf("Nb Cles : %d\n", NombreCles(a));
    }
    else if(strcmp(action, "noeudMax")  == 0)
    {
      printf("==== Noeud Max ====\n");
      printf("Noeud Max de l'arbre: \n");
      afficher_arbre(noeud_max(a), 0);
    }
    else if(strcmp(action, "clesLargeur") == 0)
    {
      printf("==== Affichage Clés Largeur ====\n");
      Afficher_Cles_Largeur(a);
    }
    else if(strcmp(action, "clesTriees") == 0)
    {
      printf("==== Afficher clés triées ====\n");
      printf("Affichage Cles Triees récursive : \n");
      Affichage_Cles_Triees_Recursive(a);
      printf("\n");
      printf("Affichage Cles Triees non récursive : \n");
      Affichage_Cles_Triees_NonRecursive(a);
      printf("\n");
    }
    else if(strcmp(action, "detruireCle") == 0)
    {
      printf("==== Détruire Clé ====\n");
      int cle;
      printf("Quelle clé souhaitez-vous supprimer ? : ");
      scanf("%d",&cle);
      printf("Arbre avant destruction clé %d \n",cle);
      afficher_arbre(a, 0);
      printf("\n");

      printf("Arbre après destruction clé %d\n",cle);
      Detruire_Cle(&a, cle);
      afficher_arbre(a, 0);
      printf("\n");
    }
    else
    printf("Erreur: commande inconnue! \n");
  }

  action = NULL;
  free(action);
  return 0;
}
