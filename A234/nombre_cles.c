#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "a234.h"

int NombreCles (Arbre234 a)
{
  if(a == NULL || a->t == 0)
  return 0 ;

  if(a->t == 2)
  {
    return a->t-1+ NombreCles (a->fils[1]) + NombreCles (a->fils[2]) ;
  }
  else
  {
    return a->t-1+ NombreCles (a->fils[0]) + NombreCles (a->fils[1])+ NombreCles (a->fils[2]) + ((a->t == 4) ? NombreCles (a->fils[3]) : 0);
  }
}
