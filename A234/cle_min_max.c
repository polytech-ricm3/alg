#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "a234.h"

int CleMax (Arbre234 a)
{
  if(a->t == 2 || a->t == 3)
  {
    if(a->fils[2]->t !=0)
    {
      return CleMax(a->fils[2]);
    }
    else
    {
      return a->cles[1];
    }
}     

  if(a->t == 4)
  {
    if(a->fils[3]->t!=0)
    {
      return CleMax(a->fils[3]);
    }
    else
    {
      return a->cles[2];
    }
  }
  return -1;
}

int CleMin (Arbre234 a)
{

  if(a->t == 2)
  {
    if(a->fils[1]->t!=0)
    {
      return CleMin(a->fils[1]);
    }
    else
    {
      return a->cles[1];
    }
  }

  if(a->t == 3 || a->t == 4)
  {
    if(a->fils[0]->t!=0)
    {
      return CleMin(a->fils[0]);
    }
    else
    {
      return a->cles[0];
    }
  }
  return -1;
}
