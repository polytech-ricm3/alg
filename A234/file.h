#define MAX_FILE_SIZE 200
#include "a234.h"

typedef struct {
  int tete ;
  int queue ;
  int nb_elem;
  Arbre234 Tab [MAX_FILE_SIZE];
} file_t, *pfile_t ;

pfile_t creer_file () ;

int file_vide (pfile_t f) ;

int file_pleine (pfile_t f) ;

Arbre234 retirer_file (pfile_t f)  ;

int deposer_file (pfile_t f, Arbre234 p) ;

Arbre234 afficher_sommet(pfile_t f);

void ajouter_arbre_file(pfile_t f, Arbre234 a, int h);
