#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "a234.h"

int position(int x, Arbre234 a)
{
  int pos, trouve = 0;
  for(pos = 0; pos < a->t && !trouve; pos++)
  {
    trouve = (x <= a->cles[pos]);
  }
  return pos;
}


Arbre234 RechercherCle (Arbre234 a, int cle)
{
  int pos;
  if(a == NULL)
    return NULL;

  pos = position(cle,a);
  if(cle == a->cles[pos])
  {
    return a;
  }
  else
  {
    return RechercherCle(a->fils[pos],cle);
  }
}
